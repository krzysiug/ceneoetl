﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Xml.XPath;
using HtmlAgilityPack;
using System.IO;
using System.Diagnostics;
using Awesomium.Core;
using System.Threading;

namespace CeneoETLConsole
{
    /// <summary>Klasa zawierająca metody do obsługi procesu ETL dla portalu OleOle!</summary>
    public class ControlOle
    {
        /// <summary>Zmienna przetrzymująca liczbę dodanych recenzji</summary>
        int ReviewAdded = 0;

        /// <summary>Metoda obsługująca proces Extract - zwraca liczbę pobranych plików </summary>
        public int Extract(string productId)
        {
            Debug.WriteLine("Starting Extract process from OleOle");
            DirectoryInfo dir = new DirectoryInfo("HtmlFilesOle/");

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                fi.Delete();
            }

            GetWebpageOle MyGetWebpageOle = new GetWebpageOle();
            
            string OleUrl = MyGetWebpageOle.GetOleUrlFromCeneoProductID(productId);
            MyGetWebpageOle.GetWebpageSourceFiles(productId, "http://ceneo.pl" + OleUrl, "HtmlFilesOle/");

            int ReviewPagesNumber = HtmlPageParser.ReviewPagesNumberOle(productId);

            GetWebpageCeneo MyGetWebpageCeneo = new GetWebpageCeneo();

            MyGetWebpageCeneo.GetWebpageSourceToFile(productId, @"HtmlFilesOle/productData.html");

            Debug.WriteLine("Extract process from OleOle ended");

            return Directory.GetFiles("HtmlFilesOle/").Count();
        }

        /// <summary>Metoda obsługująca proces Transform - zwraca produkt wraz z listą rezencji </summary>
        public ProductWithReviewList Transform()
        {
            Debug.WriteLine("Starting Transform process from Ceneo");

            GetWebpageCeneo MyGetWebpageCeneo = new GetWebpageCeneo();
            Product ParseProduct = ReviewParser.ParseProductCeneo(ParseStringToHtml.CreateHtmlFromString(MyGetWebpageCeneo.GetFileSource(@"HtmlFilesOle/productData.html")));

            GetWebpageOle MyGetWebpageOle = new GetWebpageOle();
            string[] filesArray = Directory.GetFiles("HtmlFilesOle/");
            var filesArray2 = from a in filesArray
                              where a != "HtmlFilesOle/productData.html"
                              select a;

            List < Review > myReviewList = new List<Review>();

           foreach (string sourceFile in filesArray2)
           {
                HtmlDocument myHtmlDocument = ParseStringToHtml.CreateHtmlFromString(MyGetWebpageCeneo.GetFileSource(sourceFile));
                myReviewList = myReviewList.Concat(ReviewParser.ParseHtmlDocumentOle(myHtmlDocument)).ToList();
                
           }
           ProductWithReviewList returnedProductWithReviewList = new ProductWithReviewList(ParseProduct, myReviewList);
           Debug.WriteLine("Transform process from Ceneo ended");
           return returnedProductWithReviewList;
        }

        /// <summary>Metoda obsługująca proces Load - pobiera produkt z listą rezencji i ładuje go do bazy </summary>
        public int Load(ProductWithReviewList myProduct, out bool IfExist)
        {
            ReviewAdded = 0;
            IfExist = false;
            Debug.WriteLine("Starting Load from OleOle process");
            try
            {
                using (var db = new ReviewDatabaseEntities())
                {
                    var existingProducts = from b in db.Products
                                           select b;

                    List<string> existingProductsId = new List<string>();

                    foreach (var a in existingProducts)
                    {
                        existingProductsId.Add(a.Model);
                    }

                    if (!existingProductsId.Contains(myProduct.MyProduct.Model))
                    {
                        IfExist = false;
                        Products productToadd = new Products() { Comments = myProduct.MyProduct.Comments, Model = myProduct.MyProduct.Model, Type = myProduct.MyProduct.Type, PortalProductId = myProduct.MyProduct.PortalProductId };
                        foreach (Review oneReview in myProduct.ProductReviewList)
                        {
                            Reviews reviewToAdd = new Reviews()
                            {
                                DateTimeAdded = oneReview.DateTimeAdded,
                                IsRecommended = oneReview.IsRecommended,
                                PortalReviewId = oneReview.PortalReviewId,
                                Rating = oneReview.Rating,
                                Reviewer = oneReview.Reviewer,
                                Summary = oneReview.Summary,
                                VotesOverall = oneReview.VotesOverall,
                                VotesYes = oneReview.VotesYes,
                                VotesYesPercent = oneReview.VotesYesPercent,
                                ReviewSource = oneReview.ReviewSource                                
                            };

                            foreach (string onePro in oneReview.Pros)
                            {
                                reviewToAdd.Pros.Add(new Pros() { Pros1 = onePro });
                            }

                            foreach (string oneCon in oneReview.Cons)
                            {
                                reviewToAdd.Cons.Add(new Cons() { Cons1 = oneCon });
                            }


                            productToadd.Reviews.Add(reviewToAdd);
                            ReviewAdded++;
                        }

                        db.Products.Add(productToadd);
                        db.SaveChanges();

                        
                    }
                    else
                    {
                        IfExist = true;
                        var existingProduct = (from b in db.Products
                                              where b.Model == myProduct.MyProduct.Model
                                              select b).First();
                        var existingReviews = from c in db.Reviews
                                              where c.ProductId == existingProduct.ProductId
                                              select c.PortalReviewId;

                        existingProduct = existingProduct as Products;

                        foreach (Review oneReview in myProduct.ProductReviewList)
                        {
                            if (!existingReviews.Contains(oneReview.PortalReviewId))
                            {
                                Console.WriteLine("znalazłem nową recenzję!");
                                Reviews reviewToAdd = new Reviews()
                                {
                                    DateTimeAdded = oneReview.DateTimeAdded,
                                    IsRecommended = oneReview.IsRecommended,
                                    PortalReviewId = oneReview.PortalReviewId,
                                    Rating = oneReview.Rating,
                                    Reviewer = oneReview.Reviewer,
                                    Summary = oneReview.Summary,
                                    VotesOverall = oneReview.VotesOverall,
                                    VotesYes = oneReview.VotesYes,
                                    VotesYesPercent = oneReview.VotesYesPercent,
                                    ReviewSource = oneReview.ReviewSource
                                };

                                foreach (string onePro in oneReview.Pros)
                                {
                                    reviewToAdd.Pros.Add(new Pros() { Pros1 = onePro });
                                }

                                foreach (string oneCon in oneReview.Cons)
                                {
                                    reviewToAdd.Cons.Add(new Cons() { Cons1 = oneCon });
                                }


                                existingProduct.Reviews.Add(reviewToAdd);
                                ReviewAdded++;

                            }
                        }

                        db.SaveChanges();

                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

            Debug.WriteLine("Load process from OleOle ended");

            DirectoryInfo dir = new DirectoryInfo("HtmlFilesOle/");

            foreach (FileInfo fi in dir.GetFiles())
            {
                fi.IsReadOnly = false;
                fi.Delete();
            }

            return ReviewAdded;
        }

        /// <summary>Metoda całkowicie czyszcząca bazę danych </summary>
        public void ClearDatabase(string productId)
        {
            using (var db = new ReviewDatabaseEntities())
            {
                db.Database.ExecuteSqlCommand("ALTER TABLE Reviews DROP CONSTRAINT FK_Reviews_ToTable");
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE Reviews");
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE Products");
                db.Database.ExecuteSqlCommand("ALTER TABLE Reviews ADD CONSTRAINT[FK_Reviews_ToTable] FOREIGN KEY ([ProductId]) REFERENCES[Products]([ProductId])");
            }
        }

        /// <summary>Metoda usuwająca produkt wraz z recenzjami </summary>
        public int RemoveProductReviews(string productId)
        {
            using (var db = new ReviewDatabaseEntities())
            {
                var Product = (from a in db.Products
                              where a.PortalProductId == productId
                              select a).Single();
                

                var ProductReviewList = (from b in db.Reviews
                                         where b.ProductId == Product.ProductId
                                         select b);

                foreach (Reviews c in ProductReviewList)
                {
                    var ReviewPros = (from d in db.Pros
                                      where d.ReviewId == c.ReviewId
                                      select d);
                    foreach (Pros p in ReviewPros)
                    {
                        db.Pros.Remove(p);
                    }
                    var ReviewCons = (from d in db.Cons
                                      where d.ReviewId == c.ReviewId
                                      select d);
                    foreach (Cons con in ReviewCons)
                    {
                        db.Cons.Remove(con);
                    }
                    db.Reviews.Remove(c);
                }

                db.Products.Remove(Product);

                try {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }
            }
            return 0;
        }
    }
}
