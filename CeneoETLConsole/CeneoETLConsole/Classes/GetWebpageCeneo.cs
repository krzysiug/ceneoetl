﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CeneoETLConsole
{
    /// <summary>Klasa zawierająca metody do pobierania plików źródłowych z recenzjami dla portalu Ceneo.pl</summary>
    public class GetWebpageCeneo : IGetWebpageSource
    {
        /// <summary> Metoda zwracają kod źródłowy strony z internetu- jako string </summary>
        public string GetWebpageSource(string ProductId)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string ceneoUrl = @"http://www.ceneo.pl/" + ProductId;
                //log.Info("ceneoUrl string = " + ceneoUrl);
                string htmlCode;
                try {
                    htmlCode = client.DownloadString(ceneoUrl);
                }
                catch
                {
                    throw new System.ArgumentException("Nie znaleziono produktu o podanym ID");
                }
                return htmlCode;
            }
        }

        /// <summary> Metoda zwracają kod źródłowy strony z pliku - jako plik </summary>
        public string GetFileSource(string PathToFile)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string ceneoUrl = PathToFile;
                //log.Info("ceneoUrl string = " + ceneoUrl);
                string htmlCode = client.DownloadString(ceneoUrl);
                return htmlCode;
            }
        }

        /// <summary> Metoda zwracają kod źródłowy strony z internetu - jako plik </summary>
        public void GetWebpageSourceToFile(string ProductId, string DestinationPath)
        {
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                string ceneoUrl = @"http://www.ceneo.pl/" + ProductId;
                //log.Info("ceneoUrl string = " + ceneoUrl);
                client.DownloadFile(ceneoUrl, DestinationPath);
            }
        }
    }
}
