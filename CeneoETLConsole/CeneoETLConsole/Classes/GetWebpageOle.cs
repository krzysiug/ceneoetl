﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml.XPath;
using HtmlAgilityPack;
using Awesomium.Core;
using System.Threading;
using System.IO;

namespace CeneoETLConsole
{
    /// <summary>Klasa zawierająca metody do pobierania plików źródłowych z recenzjami dla portalu OleOle!</summary>
    public class GetWebpageOle : IGetWebpageSource
    {

        
        public string GetWebpageSource(string ProductId)
        {
            using (WebClient client = new WebClient())
            {

                client.Encoding = Encoding.UTF8;
                string ceneoUrl = @"http://www.ceneo.pl" + (GetOleUrlFromCeneoProductID(ProductId));
                //log.Info("ceneoUrl string = " + ceneoUrl);
                string htmlCode;
                try
                {
                    htmlCode = client.DownloadString(ceneoUrl);
                }
                catch
                {
                    throw new System.ArgumentException("Nie znaleziono produktu o podanym ID");
                }
                return htmlCode;
            }
        }

        public int GetWebpageSourceFiles(string ProductId, string OleUrl, string DestinationPath)
        {
            int ReturnedPagesNumber = 0;

            try
            {
                WebCore.Initialize(WebConfig.Default);
            }
            catch
            {

            }
            Uri url = new Uri(OleUrl);

            using (WebSession session = WebCore.CreateWebSession(WebPreferences.Default))
            {
                using (WebView view = WebCore.CreateWebView(1100, 600, session))
                {
                    bool finishedLoading = false;
                    bool finishedResizing = false;

                    Console.WriteLine(String.Format("Loading: {0} ...", url));

                    view.Source = url;

                    view.LoadingFrameComplete += (s, e) =>
                    {
                        Console.WriteLine(String.Format("Frame Loaded: {0}", e.FrameId));
                        
                        if (e.IsMainFrame)
                            finishedLoading = true;
                    };

                    while (!finishedLoading)
                    {
                        Thread.Sleep(100);
                        WebCore.Update();
                    }

                    string viewSource = view.ExecuteJavascriptWithResult("document.documentElement.outerHTML");

                    using (StreamWriter outputFile = new StreamWriter(DestinationPath + "Ole" + ProductId + "-" + 1 + ".html"))
                    {
                        outputFile.Write(viewSource);
                        ReturnedPagesNumber++;
                    }

                    int reviewPages = HtmlPageParser.ReviewPagesNumberOle(ProductId);

                    for (int i = 2; i <= reviewPages; i++)
                    {

                        view.ExecuteJavascript("www.opinion.changePage('" + i + "')");
                        Thread.Sleep(1000);

                        viewSource = view.ExecuteJavascriptWithResult("document.documentElement.outerHTML");
                        using (StreamWriter outputFile = new StreamWriter(DestinationPath + "Ole" + ProductId + "-" + i + ".html"))
                        {
                            outputFile.Write(viewSource);
                            ReturnedPagesNumber++;
                        }
                    }




                    view.ExecuteJavascript("www.opinion.changePage('4')");
                    Thread.Sleep(1000);

                    //string viewSource = view.ExecuteJavascriptWithResult("document.documentElement.outerHTML");
                    // Print some more information.
                    Console.WriteLine(String.Format("Page Title: {0}", view.Title));
                    Console.WriteLine(String.Format("Loaded URL: {0}", view.Source));

                  //  Console.WriteLine(viewSource);




                } // Destroy and dispose the view.
                //session.Dispose();
                
            } // Release and dispose the session.            

            // Shut down Awesomium before exiting.
          //  WebCore.Shutdown();

            return ReturnedPagesNumber;
        }


        public string GetOleUrlFromCeneoProductID (string ProductId)
        {
            using (WebClient client = new WebClient())
            {
                string OleUrl = "";
                client.Encoding = Encoding.UTF8;
                string ceneoUrl = @"http://www.ceneo.pl/" + ProductId;
                //log.Info("ceneoUrl string = " + ceneoUrl);
                string htmlCode;
                try
                {
                    htmlCode = client.DownloadString(ceneoUrl);
                    HtmlAgilityPack.HtmlDocument MyHtmlDocument = new HtmlAgilityPack.HtmlDocument();
                    MyHtmlDocument.LoadHtml(htmlCode);

                    try {
                        var ascensors = MyHtmlDocument.DocumentNode.SelectSingleNode("//img[@alt='oleole.pl']").Ancestors();
                        var aNode = (from element in ascensors
                                     where element.Name == "a"
                                     select element.Attributes["href"].Value).First().ToString();
                        OleUrl = aNode;
                    }
                    catch
                    {
                        htmlCode = client.DownloadString(ceneoUrl + "clr");
                        MyHtmlDocument.LoadHtml(htmlCode);
                        var ascensors = MyHtmlDocument.DocumentNode.SelectSingleNode("//img[@alt='oleole.pl']").Ancestors();
                        var aNode = (from element in ascensors
                                     where element.Name == "a"
                                     select element.Attributes["href"].Value).First().ToString();
                        OleUrl = aNode;
                    }
                }
                catch
                {
                    throw new System.ArgumentException("Nie znaleziono produktu o podanym ID na stronie OleOle");
                }
                
                return OleUrl;
            }
        }
    }
}
