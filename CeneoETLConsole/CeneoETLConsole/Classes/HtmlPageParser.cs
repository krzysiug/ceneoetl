﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Xml.XPath;
using HtmlAgilityPack;

namespace CeneoETLConsole
{
    /// <summary> Klasa zawierająca pomocnicze metody do parsowania plików html </summary>
    public static class HtmlPageParser
    {
        /// <summary> Metoda zwracają czy podana podstrona zawiera link do kolejnej strony z opiniami </summary>
        public static bool IfContainsNext (HtmlDocument DocumentToParse)
        {
            if (DocumentToParse.DocumentNode.SelectNodes("//li[@class='page-arrow arrow-next']") != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> Metoda zwracają ilość stron z opiniami z portalu Ceneo</summary>
        public static int ReviewPagesNumber(string ProductId)
        {
            GetWebpageCeneo MyGetWebpageCeneo = new GetWebpageCeneo();
            int i = 1;
            while (true)
            {
                if (IfContainsNext(ParseStringToHtml.CreateHtmlFromString(MyGetWebpageCeneo.GetWebpageSource(ProductId + @"/opinie-" + i))))
                    i++;
                else break;
            }
            return i;
        }

        /// <summary> Metoda zwracają ilość stron z opiniami z portalu OleOle</summary>
        public static int ReviewPagesNumberOle(string ProductId)
        {
            GetWebpageOle MyGetWebpageOle = new GetWebpageOle();
            int i = 1;

            HtmlDocument OleHtmlDocument = ParseStringToHtml.CreateHtmlFromString(MyGetWebpageOle.GetWebpageSource(ProductId));

            var OleAllANoedes = OleHtmlDocument.DocumentNode.SelectNodes("//a[@href='javascript:void(0)']");
            try {
                var OleReviewNodes = (from element in OleAllANoedes
                                      where element.OuterHtml.Contains("changePage") && element.OuterHtml.Contains("page-navigator-button")
                                      select Int16.Parse(element.InnerText)).Max();
                return OleReviewNodes;
            }
            catch (Exception e)
            {
            }
            return 1;
        }
    }
}
