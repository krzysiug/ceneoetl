﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary> Interfejs do pobierania strony jako string </summary>
    public interface IGetWebpageSource
    {
        string GetWebpageSource(string ProductId);
    }
}
