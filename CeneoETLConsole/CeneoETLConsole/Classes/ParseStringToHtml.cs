﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Xml.XPath;
using HtmlAgilityPack;

namespace CeneoETLConsole
{
    /// <summary> Klasa zawierająca metodę do konwersji stringa do HtmlDocument </summary>
    public static class ParseStringToHtml
    {
        /// <summary> Metoda robiąca konwersję stringa do HtmlDocument </summary>
        public static HtmlDocument CreateHtmlFromString (string HtmlSource)
        {
            HtmlDocument MyHtmlDocument = new HtmlDocument();
            MyHtmlDocument.LoadHtml(HtmlSource);
            return MyHtmlDocument;
       }        
    }
}
