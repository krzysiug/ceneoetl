﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary> Klasa reprezentująca produkt </summary>
    public class Product
    {
        /// <summary> Id produktu z portalu </summary>
        public string PortalProductId { get; set; }
        /// <summary> Typ produktu </summary>
        public string Type { get; set; }
        /// <summary> Model produktu </summary>
        public string Model { get; set; }
        /// <summary> Komentarze produktu </summary>
        public string Comments { get; set; }

        /// <summary> Konsturktor produktu </summary>
        public Product(string PortalProductId, string Type, string Model, string Comments)
        {
            this.PortalProductId = PortalProductId;
            this.Type = Type;
            this.Model = Model;
            this.Comments = Comments;
        }

        /// <summary> Przeciążona metoda wypisująca produkt </summary>
        public override string ToString()
        {
            string ProductToString = "";
            ProductToString += "Id produktu z portalu ceneo:" + PortalProductId;
            ProductToString += Environment.NewLine;
            ProductToString += "Typ produktu:" + Type;
            ProductToString += Environment.NewLine;
            ProductToString += "Model produktu:" + Model;
            ProductToString += Environment.NewLine;
            ProductToString += "Opis produktu:" + Comments;
            ProductToString += Environment.NewLine;
            return ProductToString;
        }
    }
}
