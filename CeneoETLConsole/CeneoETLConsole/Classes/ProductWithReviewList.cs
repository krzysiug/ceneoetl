﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary> Klasa reprezentująca produkt wraz z listą rezencji </summary>
    public class ProductWithReviewList
    {
        /// <summary> Produkt jako składnik klasy </summary>
        public Product MyProduct { get; set; }
        /// <summary> Lista recenzji jako składnik klasy </summary>
        public List<Review> ProductReviewList { get; set; }

        /// <summary> Konsturktor obiektu </summary>
        public ProductWithReviewList(Product MyProduct, List<Review> ProductReviewList)
        {
            this.MyProduct = MyProduct;
            this.ProductReviewList = ProductReviewList;
        }
    }
}
