﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary>Klasa reprezentująca produkt z listą recenzji do wyświetlenia</summary>
    public class ProductWithReviewListToDisplay
    {
        /// <summary> Id produktu z portalu </summary>
        public string  PortalProductId { get; set; }
        /// <summary> Typ produktu </summary>
        public string  Type { get; set; }
        /// <summary> Model produktu </summary>
        public string  Model { get; set; }
        /// <summary> Komentarze produktu </summary>
        public string  Comments { get; set; }

        /// <summary>Lista recenzji do wyświetlenia</summary>
        public List<ReviewToDisplay> Reviews { get; set; }

        /// <summary>Konstruktor obiektu</summary>
        public ProductWithReviewListToDisplay(string ProductId)
        {
            Reviews = new List<ReviewToDisplay>();
            using (var db = new ReviewDatabaseEntities())
            {
                try {
                    var Product = (from a in db.Products
                                   where a.PortalProductId == ProductId
                                   select a).Single();

                    PortalProductId = Product.PortalProductId;
                    Type = Product.Type;
                    Model = Product.Model;
                    Comments = Product.Comments;

                    foreach (var Review in Product.Reviews)
                    {
                        string Pros = "";
                        string Cons = "";

                        foreach (var Pro in Review.Pros)
                        {
                            Pros += Pro.Pros1 + " ; ";
                        }

                        foreach (var Con in Review.Cons)
                        {
                            Cons += Con.Cons1 + " ; ";
                        }

                        string PortalReviewId = Review.PortalReviewId;
                        string Summary = Review.Summary;
                        string Rating = Review.Rating.ToString();
                        string Reviewer = Review.Reviewer;
                        string DateTimeAdded = Review.DateTimeAdded.ToString();
                        string IsRecommended = Review.IsRecommended.ToString();
                        string VotesYesPercent = Review.VotesYesPercent.ToString();
                        string VotesYes = Review.VotesYes.ToString();
                        string VotesOverall = Review.VotesOverall.ToString();
                        string ReviewSource = Review.ReviewSource;

                        Reviews.Add(new ReviewToDisplay(PortalReviewId, Pros, Cons, Summary, Rating, Reviewer, DateTimeAdded, IsRecommended, VotesYesPercent, VotesYes, VotesOverall, ReviewSource));

                    }

                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Nie udało się odczytać produktu o podanym ID!");
                }
            }
        }

        /// <summary>Przeciążona metoda wyświetlająca obiekt</summary>
        public override string ToString()
        {
            string ProductToString = "";
            ProductToString += "Id produktu z portalu ceneo:" + PortalProductId;
            ProductToString += Environment.NewLine;
            ProductToString += "Typ produktu:" + Type;
            ProductToString += Environment.NewLine;
            ProductToString += "Model produktu:" + Model;
            ProductToString += Environment.NewLine;
            ProductToString += "Opis produktu:" + Comments;
            ProductToString += Environment.NewLine;
            return ProductToString;
        }
    }


}
