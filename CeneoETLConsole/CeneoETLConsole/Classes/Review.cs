﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary>Klasa, która reprezentuję rezencję w programie</summary>
    public class Review
    {   
        /// <summary>Id produktu z portalu</summary>
        public string PortalReviewId
        {
            get; set;
        }

        /// <summary>Lista zawierająca zalety produktu</summary>
        public List<string> Pros
        {
            get; set;
        }

        /// <summary>Lista zawierająca wady produktu</summary>
        public List<string> Cons
        {
            get; set;
        }

        /// <summary>Podsumowanie produktu</summary>
        public string Summary
        {
            get; set;
        }

        /// <summary>Liczba gwiazdek przyznanych produktowi</summary>
        public double Rating
        {
            get; set;
        }

        /// <summary>Podpis recenzenta</summary>
        public string Reviewer
        {
            get; set;
        }

        /// <summary>Czas dodania produktu</summary>
        public DateTime DateTimeAdded
        {
            get; set;
        }

        /// <summary>Czy produkt jest rekomendowany przez recenzenta</summary>
        public bool IsRecommended
        {
            get; set;
        }

        /// <summary>Procent osób uznających rezenzję za przydatną</summary>
        public int VotesYesPercent
        {
            get; set;
        }

        /// <summary>Liczba osób uznających rezenzję za przydatną</summary>
        public int VotesYes
        {
            get; set;
        }

        /// <summary>Liczba osób oceniających przydatność recenzji</summary>
        public int VotesOverall
        {
            get; set;
        }

        /// <summary>Źródło recenzji</summary>
        public string ReviewSource
        {
            get; set;
        }

        /// <summary>Kontruktor obiektu Recenzji</summary>
        public Review(string CeneoReviewId, List<string> Pros, List<string> Cons, string Summary, double Rating, string Reviewer, DateTime DateTimeAdded, bool IsRecommended, int VotesYesPercent, int VotesYes, int VotesOverall, string ReviewSource)
        {
            this.PortalReviewId = CeneoReviewId;
            this.Pros = Pros;
            this.Cons = Cons;
            this.Summary = Summary;
            this.Rating = Rating;
            this.Reviewer = Reviewer;
            this.DateTimeAdded = DateTimeAdded;
            this.IsRecommended = IsRecommended;
            this.VotesYesPercent = VotesYesPercent;
            this.VotesYes = VotesYes;
            this.VotesOverall = VotesOverall;
            this.ReviewSource = ReviewSource;
        }

        /// <summary>Przeciążona metoda do wyświetlania obketu w konsoli</summary>
        public override string ToString()
        {
            string ReviewToString = "";
            ReviewToString += PortalReviewId.ToString();
            foreach (string el in Pros)
            {
                ReviewToString += " " + el;
            }
            foreach (string el in Cons)
            {
                ReviewToString += " " + el;
            }
            ReviewToString += " " + Summary;
            ReviewToString += " " + Reviewer;
            ReviewToString += " " + DateTimeAdded;
            ReviewToString += "\n--------------------------------------";
            return ReviewToString;
        }
    }
}
