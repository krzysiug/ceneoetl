﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Xml.XPath;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
namespace CeneoETLConsole
{
    /// <summary>Klasa zawierająca metody do parsowania dokumentów html</summary>
    public static class ReviewParser
    {
        /// <summary>Metoda zamienianiąca HtmlDocument na listę recenzji (obiektów typu Review) dla portalu Ceneo.pl</summary>
        public static List<Review> ParseHtmlDocumentCeneo(HtmlDocument DocumentToParse)
        {
            List<Review> ReviewList = new List<Review>();
            if (DocumentToParse.DocumentNode.SelectNodes("//li[@class='product-review js_product-review']") != null)
            {
                foreach (HtmlNode node in DocumentToParse.DocumentNode.SelectNodes("//li[@class='product-review js_product-review']"))
                {
                    
                    string Reviewer;
                    string Summary;
                    double Rating;
                    bool IsRecommended;
                    DateTime DateTimeAdded;
                    List<string> Prons = new List<string>();
                    List<string> Cons = new List<string>();
                    string ReviewCeneoId;
                    int VotesYesPercent;
                    int VotesYes;
                    int VotesOverall;

                    try
                    {
                        foreach (HtmlNode liNode in node.SelectNodes(".//span[@class='cons-cell']//li"))
                        {
                            Cons.Add(liNode.InnerHtml.Trim());
                        }
                    }
                    catch
                    {

                    }
                    try
                    {

                        foreach (HtmlNode liNode in node.SelectNodes(".//span[@class='pros-cell']//li"))
                        {
                            Prons.Add(liNode.InnerHtml.Trim());
                        }
                    }
                    catch
                    {

                    }

                    ReviewCeneoId = node.SelectSingleNode(".//a[@class='report-product-review-abuse review-link js_report-product-review-abuse']").Attributes["data-review-id"].Value;

                    Summary = node.SelectSingleNode(".//p[@class='product-review-body']").InnerHtml;

                    Rating = Double.Parse(node.SelectSingleNode(".//span[@class='review-score-count']").InnerHtml.Split('/')[0]);

                    Reviewer = node.SelectSingleNode(".//div[@class='product-reviewer']").InnerHtml.Trim();

                    DateTimeAdded = DateTime.ParseExact(node.SelectSingleNode(".//time").Attributes["datetime"].Value, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    try
                    {
                        IsRecommended = (node.SelectSingleNode(".//div[@class='product-review-summary']//em").Attributes["class"].Value == "product-recommended") ? true : false;
                    }
                    catch
                    {
                        IsRecommended = false;
                    }
                    
                    string VotesYesPercentString = ".//span[@id='votes-yes-percent-" + ReviewCeneoId.ToString() + "']";
                    VotesYesPercent = int.Parse(node.SelectSingleNode(VotesYesPercentString).InnerHtml);

                    string VotesYesString = ".//span[@id='votes-yes-" + ReviewCeneoId.ToString() + "']";
                    VotesYes = int.Parse(node.SelectSingleNode(VotesYesString).InnerHtml);


                    string VotesOverallString = ".//span[@id='votes-" + ReviewCeneoId.ToString() + "']";
                    VotesOverall = int.Parse(node.SelectSingleNode(VotesOverallString).InnerHtml);

                    Review newReview = new Review(ReviewCeneoId, Prons, Cons, Summary, Rating, Reviewer, DateTimeAdded, IsRecommended, VotesYesPercent, VotesYes, VotesOverall, "Ceneo");
                    ReviewList.Add(newReview);
                    //Console.WriteLine(newReview.ToString());
                }
            }
            return ReviewList;
        }

        /// <summary> 	Metoda zamienianiąca HtmlDocument na listę recenzji (obiektów typu Review) dla portalu OleOle!</summary>
        public static List<Review> ParseHtmlDocumentOle(HtmlDocument DocumentToParse)
        {
            List<Review> ReviewList = new List<Review>();

            try {
                var HtmlNodes = DocumentToParse.DocumentNode.SelectNodes("//div[@class='item' and contains(@id, 'opinion')]");

                foreach (var ReviewNode in HtmlNodes)
                {
                    string Reviewer;
                    string Summary;
                    double Rating = 0;
                    bool IsRecommended = false;
                    DateTime DateTimeAdded;
                    List<string> Prons = new List<string>();
                    List<string> Cons = new List<string>();
                    string ReviewOleId;
                    int VotesYesPercent;
                    int VotesYes;
                    int VotesOverall;

                    string reviewHtmlString = ReviewNode.SelectSingleNode(".//span[@class='votesNum']").Attributes["id"].Value;
                    ReviewOleId = reviewHtmlString.Substring(8);

                    DateTimeAdded = DateTime.ParseExact(ReviewNode.SelectSingleNode(".//div[@class='dateAdded']//span").InnerText + " 00:00:00", "dd-MM-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                    Reviewer = ReviewNode.SelectSingleNode(".//div[@class='nick']//span").InnerText;

                    var ProsAndConsDiv = ReviewNode.SelectNodes(".//div[@class='item']//div[@class='stars']");
                    foreach (var ProCon in ProsAndConsDiv)
                    {
                        string Attribute = ProCon.SelectSingleNode("../span").InnerText.TrimEnd(':');
                        string Star = ProCon.Attributes["title"].Value;

                        switch (Star)
                        {
                            case "rewelacyjny":
                                Prons.Add(Attribute);
                                break;
                            case "dobry":
                                Prons.Add(Attribute);
                                break;
                            case "wystarczający":
                                Cons.Add(Attribute);
                                break;
                            case "nieudany":
                                Cons.Add(Attribute);
                                break;
                            default:
                                break;

                        }
                    }

                    string RatingString = ReviewNode.SelectSingleNode(".//div[@class='bigStars']").Attributes["title"].Value;
                    switch (RatingString)
                    {
                        case "rewelacyjny":
                            Rating = 5;
                            break;
                        case "dobry":
                            Rating = 4;
                            break;
                        case "w porządku":
                            Rating = 3;
                            break;
                        case "wystarczający":
                            Rating = 2;
                            break;
                        case "nieudany":
                            Rating = 1;
                            break;
                        default:
                            break;
                    }

                    if (Rating >= 3)
                        IsRecommended = true;

                    var ReviewParahraphNodes = ReviewNode.SelectNodes(".//div[contains(@class, 'fullOpinion')]");

                    var ReviewParagraphText = from element in ReviewParahraphNodes
                                              select element.InnerText;

                    Summary = string.Join("", ReviewParagraphText).Trim();
                    Summary = Regex.Replace(Summary, @"\s+", " ");
                    Summary = Summary.Replace(@"« zwiń", "");
                    VotesYesPercent = Int16.Parse(ReviewNode.SelectSingleNode(".//span[@class='votesNum']//span").InnerText);
                    VotesYes = Int16.Parse(ReviewNode.SelectSingleNode(".//span[@class='yes']").InnerText); 
                    VotesOverall = Int16.Parse(ReviewNode.SelectSingleNode(".//span[@class='yes']").InnerText) + Int16.Parse(ReviewNode.SelectSingleNode(".//span[@class='no']").InnerText);

                    Review newReview = new Review(ReviewOleId, Prons, Cons, Summary, Rating, Reviewer, DateTimeAdded, IsRecommended, VotesYesPercent, VotesYes, VotesOverall, "OleOle");
                    ReviewList.Add(newReview);
                }

                // var opinionNodes = from element in 
                //div[contains(@class,'atag') and contains(@class ,'btag')]
            }
            catch
            {
                throw new ApplicationException("Wystąpił błąd podczas przetwarzania plików");
            }
            return ReviewList;
        }
        /// <summary>Metoda wyodrębniająca produkt z obiektu typu HtmlDocument</summary>
        public static Product ParseProductCeneo(HtmlDocument DocumentToParse)
        {
            string PortalProductId = null;
            string Type = null;
            string Model = null;
            string Comments = null;

            try {
                HtmlNode node = DocumentToParse.DocumentNode.SelectSingleNode("//nav[@class='breadcrumbs']//dl//dd");

                PortalProductId = DocumentToParse.DocumentNode.SelectSingleNode("//input[@name='returnUrl']").Attributes["value"].Value.Substring(1);
                var spanColl = node.SelectNodes(".//span");
                Type = spanColl.Last().InnerHtml;

                Model = node.SelectSingleNode(".//strong").InnerHtml;

                node = DocumentToParse.DocumentNode.SelectSingleNode("//div[@class='product-desc-text']");
                Comments = node.InnerHtml;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }

            Product returnedProduct = new Product(PortalProductId, Type,  Model, Comments);
            return returnedProduct;
        }
    }
}
