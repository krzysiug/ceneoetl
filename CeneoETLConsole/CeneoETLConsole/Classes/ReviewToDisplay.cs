﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CeneoETLConsole
{
    /// <summary>Klasa reprezentująca recenzję do wyświetlenia</summary>
    public class ReviewToDisplay
    {
        /// <summary>Id produktu</summary>
        public string PortalReviewId { get; set; }
        /// <summary>Zalety produktu</summary>
        public string Pros { get; set; }
        /// <summary>Wady produktu</summary>
        public string Cons { get; set; }
        /// <summary>Podsumowanie opinii</summary>
        public string Summary { get; set; }
        /// <summary>Ocena produktu</summary>
        public string Rating { get; set; }
        /// <summary>Nick recenzenta</summary>
        public string Reviewer { get; set; }
        /// <summary>Czas dodania recenzji</summary>
        public string DateTimeAdded { get; set; }
        /// <summary>Czy recenzujący rekomenduje produkt</summary>
        public string IsRecommended { get; set; }
        /// <summary>Procent osób głosujących na tak</summary>
        public string VotesYesPercent { get; set; }
        /// <summary>Liczba głosów na tak</summary>
        public string VotesYes { get; set; }
        /// <summary>Liczba głosujących w ogóle</summary>
        public string VotesOverall { get; set; }
        /// <summary>Źródło recenzji</summary>
        public string ReviewSource { get; set; }

        /// <summary>Konstruktor obiektu</summary>
        public ReviewToDisplay(string PortalReviewId, string Pros, string Cons, 
                               string Summary, string Rating, string Reviewer, 
                               string DateTimeAdded, string IsRecommended, string VotesYesPercent,
                               string VotesYes, string VotesOverall, string ReviewSource)
        {
            this.PortalReviewId = PortalReviewId;
            this.Pros = Pros;
            this.Cons = Cons;
            this.Summary = Summary;
            this.Rating = Rating;
            this.Reviewer = Reviewer;
            this.DateTimeAdded = DateTimeAdded;
            this.IsRecommended = IsRecommended;
            this.VotesYesPercent = VotesYesPercent;
            this.VotesYes = VotesYes;
            this.VotesOverall = VotesOverall;
            this.ReviewSource = ReviewSource;
        }

        /// <summary>Przeciążona metoda wyświetlająca obiekt</summary>
        public override string ToString()
        {
            string ReviewToString = "";
            ReviewToString += "Id recenzji z portalu: " + PortalReviewId;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Zalety produktu: " + Pros;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Wady produktu: " + Cons;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Podsumowanie recenzji: " + Summary;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Ocena produktu: " + Rating;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Nick oceniającego: " + Reviewer;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Czas dodania recenzji: " + DateTimeAdded;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Czy produkt jest rekomendowany: " + IsRecommended;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Procent osób uznających opinię za przydatną: " + VotesYesPercent;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Liczba osób uznających opinię za przydatną: " + VotesYes;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Liczba osób oceniająca recenzję: " + VotesOverall;
            ReviewToString += Environment.NewLine;
            ReviewToString += "Źródło recenzji: " + ReviewSource;
            ReviewToString += Environment.NewLine;
            return ReviewToString;
        }
    }
}
