﻿namespace CeneoETLConsole
{
    partial class Form1
    {
        //// <summary>
        //// Required designer variable.
        //// </summary>
        private System.ComponentModel.IContainer components = null;

        //// <summary>
        //// Clean up any resources being used.
        //// </summary>
        //// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        //// <summary>
        //// Required method for Designer support - do not modify
        //// the contents of this method with the code editor.
        //// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.reviewsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ExportButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ProductDisplaySummary = new System.Windows.Forms.TextBox();
            this.ProductDisplayType = new System.Windows.Forms.TextBox();
            this.ProductDisplayName = new System.Windows.Forms.TextBox();
            this.ProductToDisplayTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.ETLButtonOle = new System.Windows.Forms.Button();
            this.LoadButtonOle = new System.Windows.Forms.Button();
            this.TransformButtonOle = new System.Windows.Forms.Button();
            this.ExtractButtonOle = new System.Windows.Forms.Button();
            this.CeneoTextLabel = new System.Windows.Forms.Label();
            this.ClearLogTextBox = new System.Windows.Forms.Button();
            this.DeleteProductReviews = new System.Windows.Forms.Button();
            this.LogTextBox = new System.Windows.Forms.TextBox();
            this.ProductIdTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ETLButton = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.TransformButton = new System.Windows.Forms.Button();
            this.ExtractButton = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.reviewDatabaseDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reviewDatabaseDataSet = new CeneoETLConsole.ReviewDatabaseDataSet();
            this.productsTableAdapter = new CeneoETLConsole.ReviewDatabaseDataSetTableAdapters.ProductsTableAdapter();
            this.reviewsTableAdapter = new CeneoETLConsole.ReviewDatabaseDataSetTableAdapters.ReviewsTableAdapter();
            this.portalReviewIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prosDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ratingDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reviewerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateTimeAddedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isRecommendedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.votesYesPercentDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.votesYesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.votesOverallDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reviewSourceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reviewToDisplayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productWithReviewListToDisplayBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.productWithReviewListToDisplayBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.reviewsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reviewDatabaseDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewDatabaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewToDisplayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productWithReviewListToDisplayBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productWithReviewListToDisplayBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // reviewsBindingSource
            // 
            this.reviewsBindingSource.DataMember = "Reviews";
            this.reviewsBindingSource.DataSource = this.reviewDatabaseDataSetBindingSource;
            // 
            // productsBindingSource
            // 
            this.productsBindingSource.DataMember = "Products";
            this.productsBindingSource.DataSource = this.reviewDatabaseDataSetBindingSource;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ExportButton);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Controls.Add(this.ProductDisplaySummary);
            this.tabPage2.Controls.Add(this.ProductDisplayType);
            this.tabPage2.Controls.Add(this.ProductDisplayName);
            this.tabPage2.Controls.Add(this.ProductToDisplayTextBox);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1304, 624);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Wyniki";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ExportButton
            // 
            this.ExportButton.Location = new System.Drawing.Point(422, -1);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(72, 22);
            this.ExportButton.TabIndex = 16;
            this.ExportButton.Text = "Eksportuj";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.portalReviewIdDataGridViewTextBoxColumn,
            this.prosDataGridViewTextBoxColumn,
            this.consDataGridViewTextBoxColumn,
            this.summaryDataGridViewTextBoxColumn,
            this.ratingDataGridViewTextBoxColumn,
            this.reviewerDataGridViewTextBoxColumn,
            this.dateTimeAddedDataGridViewTextBoxColumn,
            this.isRecommendedDataGridViewTextBoxColumn,
            this.votesYesPercentDataGridViewTextBoxColumn,
            this.votesYesDataGridViewTextBoxColumn,
            this.votesOverallDataGridViewTextBoxColumn,
            this.reviewSourceDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.reviewToDisplayBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 103);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1271, 457);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ProductDisplaySummary
            // 
            this.ProductDisplaySummary.Location = new System.Drawing.Point(137, 74);
            this.ProductDisplaySummary.Name = "ProductDisplaySummary";
            this.ProductDisplaySummary.ReadOnly = true;
            this.ProductDisplaySummary.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.ProductDisplaySummary.Size = new System.Drawing.Size(898, 20);
            this.ProductDisplaySummary.TabIndex = 14;
            // 
            // ProductDisplayType
            // 
            this.ProductDisplayType.Location = new System.Drawing.Point(137, 48);
            this.ProductDisplayType.Name = "ProductDisplayType";
            this.ProductDisplayType.ReadOnly = true;
            this.ProductDisplayType.Size = new System.Drawing.Size(250, 20);
            this.ProductDisplayType.TabIndex = 13;
            // 
            // ProductDisplayName
            // 
            this.ProductDisplayName.Location = new System.Drawing.Point(137, 22);
            this.ProductDisplayName.Name = "ProductDisplayName";
            this.ProductDisplayName.ReadOnly = true;
            this.ProductDisplayName.Size = new System.Drawing.Size(250, 20);
            this.ProductDisplayName.TabIndex = 12;
            // 
            // ProductToDisplayTextBox
            // 
            this.ProductToDisplayTextBox.Location = new System.Drawing.Point(137, 0);
            this.ProductToDisplayTextBox.Name = "ProductToDisplayTextBox";
            this.ProductToDisplayTextBox.Size = new System.Drawing.Size(177, 20);
            this.ProductToDisplayTextBox.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Opis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Grupa produktów";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Produkt";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(320, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 21);
            this.button1.TabIndex = 8;
            this.button1.Text = "Ładuj opinie";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Numer produktu z Ceneo:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.ETLButtonOle);
            this.tabPage1.Controls.Add(this.LoadButtonOle);
            this.tabPage1.Controls.Add(this.TransformButtonOle);
            this.tabPage1.Controls.Add(this.ExtractButtonOle);
            this.tabPage1.Controls.Add(this.CeneoTextLabel);
            this.tabPage1.Controls.Add(this.ClearLogTextBox);
            this.tabPage1.Controls.Add(this.DeleteProductReviews);
            this.tabPage1.Controls.Add(this.LogTextBox);
            this.tabPage1.Controls.Add(this.ProductIdTextBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.ETLButton);
            this.tabPage1.Controls.Add(this.LoadButton);
            this.tabPage1.Controls.Add(this.TransformButton);
            this.tabPage1.Controls.Add(this.ExtractButton);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1304, 624);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ETL";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(92, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "OleOle!";
            // 
            // ETLButtonOle
            // 
            this.ETLButtonOle.Location = new System.Drawing.Point(154, 118);
            this.ETLButtonOle.Name = "ETLButtonOle";
            this.ETLButtonOle.Size = new System.Drawing.Size(269, 23);
            this.ETLButtonOle.TabIndex = 13;
            this.ETLButtonOle.Text = "ETL";
            this.ETLButtonOle.UseVisualStyleBackColor = true;
            this.ETLButtonOle.Click += new System.EventHandler(this.ETLButtonOle_Click);
            // 
            // LoadButtonOle
            // 
            this.LoadButtonOle.Location = new System.Drawing.Point(348, 147);
            this.LoadButtonOle.Name = "LoadButtonOle";
            this.LoadButtonOle.Size = new System.Drawing.Size(75, 23);
            this.LoadButtonOle.TabIndex = 12;
            this.LoadButtonOle.Text = "Load";
            this.LoadButtonOle.UseVisualStyleBackColor = true;
            this.LoadButtonOle.Click += new System.EventHandler(this.LoadButtonOle_Click);
            // 
            // TransformButtonOle
            // 
            this.TransformButtonOle.Location = new System.Drawing.Point(249, 147);
            this.TransformButtonOle.Name = "TransformButtonOle";
            this.TransformButtonOle.Size = new System.Drawing.Size(75, 23);
            this.TransformButtonOle.TabIndex = 11;
            this.TransformButtonOle.Text = "Transform";
            this.TransformButtonOle.UseVisualStyleBackColor = true;
            this.TransformButtonOle.Click += new System.EventHandler(this.TransformButtonOle_Click);
            // 
            // ExtractButtonOle
            // 
            this.ExtractButtonOle.Location = new System.Drawing.Point(154, 147);
            this.ExtractButtonOle.Name = "ExtractButtonOle";
            this.ExtractButtonOle.Size = new System.Drawing.Size(75, 23);
            this.ExtractButtonOle.TabIndex = 10;
            this.ExtractButtonOle.Text = "Extract";
            this.ExtractButtonOle.UseVisualStyleBackColor = true;
            this.ExtractButtonOle.Click += new System.EventHandler(this.ExtractButtonOle_Click);
            // 
            // CeneoTextLabel
            // 
            this.CeneoTextLabel.AutoSize = true;
            this.CeneoTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CeneoTextLabel.Location = new System.Drawing.Point(97, 60);
            this.CeneoTextLabel.Name = "CeneoTextLabel";
            this.CeneoTextLabel.Size = new System.Drawing.Size(56, 20);
            this.CeneoTextLabel.TabIndex = 9;
            this.CeneoTextLabel.Text = "Ceneo";
            // 
            // ClearLogTextBox
            // 
            this.ClearLogTextBox.Location = new System.Drawing.Point(463, 427);
            this.ClearLogTextBox.Name = "ClearLogTextBox";
            this.ClearLogTextBox.Size = new System.Drawing.Size(82, 23);
            this.ClearLogTextBox.TabIndex = 8;
            this.ClearLogTextBox.Text = "Wyczyść logi";
            this.ClearLogTextBox.UseVisualStyleBackColor = true;
            this.ClearLogTextBox.Click += new System.EventHandler(this.ClearLogTextBox_Click);
            // 
            // DeleteProductReviews
            // 
            this.DeleteProductReviews.Location = new System.Drawing.Point(429, 34);
            this.DeleteProductReviews.Name = "DeleteProductReviews";
            this.DeleteProductReviews.Size = new System.Drawing.Size(75, 23);
            this.DeleteProductReviews.TabIndex = 7;
            this.DeleteProductReviews.Text = "Usuń opinie";
            this.DeleteProductReviews.UseVisualStyleBackColor = true;
            this.DeleteProductReviews.Click += new System.EventHandler(this.DeleteProductReviews_Click);
            // 
            // LogTextBox
            // 
            this.LogTextBox.Location = new System.Drawing.Point(33, 171);
            this.LogTextBox.Multiline = true;
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.ReadOnly = true;
            this.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogTextBox.Size = new System.Drawing.Size(512, 250);
            this.LogTextBox.TabIndex = 6;
            // 
            // ProductIdTextBox
            // 
            this.ProductIdTextBox.Location = new System.Drawing.Point(246, 34);
            this.ProductIdTextBox.Name = "ProductIdTextBox";
            this.ProductIdTextBox.Size = new System.Drawing.Size(177, 20);
            this.ProductIdTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(50, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Numer produktu z Ceneo:";
            // 
            // ETLButton
            // 
            this.ETLButton.Location = new System.Drawing.Point(154, 60);
            this.ETLButton.Name = "ETLButton";
            this.ETLButton.Size = new System.Drawing.Size(269, 23);
            this.ETLButton.TabIndex = 3;
            this.ETLButton.Text = "ETL";
            this.ETLButton.UseVisualStyleBackColor = true;
            this.ETLButton.Click += new System.EventHandler(this.ETLButton_Click);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(348, 89);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 2;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // TransformButton
            // 
            this.TransformButton.Location = new System.Drawing.Point(249, 89);
            this.TransformButton.Name = "TransformButton";
            this.TransformButton.Size = new System.Drawing.Size(75, 23);
            this.TransformButton.TabIndex = 1;
            this.TransformButton.Text = "Transform";
            this.TransformButton.UseVisualStyleBackColor = true;
            this.TransformButton.Click += new System.EventHandler(this.TransformButton_Click);
            // 
            // ExtractButton
            // 
            this.ExtractButton.Location = new System.Drawing.Point(154, 89);
            this.ExtractButton.Name = "ExtractButton";
            this.ExtractButton.Size = new System.Drawing.Size(75, 23);
            this.ExtractButton.TabIndex = 0;
            this.ExtractButton.Text = "Extract";
            this.ExtractButton.UseVisualStyleBackColor = true;
            this.ExtractButton.Click += new System.EventHandler(this.ExtractButton_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1312, 650);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // reviewDatabaseDataSetBindingSource
            // 
            this.reviewDatabaseDataSetBindingSource.DataSource = this.reviewDatabaseDataSet;
            this.reviewDatabaseDataSetBindingSource.Position = 0;
            // 
            // reviewDatabaseDataSet
            // 
            this.reviewDatabaseDataSet.DataSetName = "ReviewDatabaseDataSet";
            this.reviewDatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // productsTableAdapter
            // 
            this.productsTableAdapter.ClearBeforeFill = true;
            // 
            // reviewsTableAdapter
            // 
            this.reviewsTableAdapter.ClearBeforeFill = true;
            // 
            // portalReviewIdDataGridViewTextBoxColumn
            // 
            this.portalReviewIdDataGridViewTextBoxColumn.DataPropertyName = "PortalReviewId";
            this.portalReviewIdDataGridViewTextBoxColumn.HeaderText = "Id Recenzji";
            this.portalReviewIdDataGridViewTextBoxColumn.Name = "portalReviewIdDataGridViewTextBoxColumn";
            // 
            // prosDataGridViewTextBoxColumn
            // 
            this.prosDataGridViewTextBoxColumn.DataPropertyName = "Pros";
            this.prosDataGridViewTextBoxColumn.HeaderText = "Zalety";
            this.prosDataGridViewTextBoxColumn.Name = "prosDataGridViewTextBoxColumn";
            // 
            // consDataGridViewTextBoxColumn
            // 
            this.consDataGridViewTextBoxColumn.DataPropertyName = "Cons";
            this.consDataGridViewTextBoxColumn.HeaderText = "Wady";
            this.consDataGridViewTextBoxColumn.Name = "consDataGridViewTextBoxColumn";
            // 
            // summaryDataGridViewTextBoxColumn
            // 
            this.summaryDataGridViewTextBoxColumn.DataPropertyName = "Summary";
            this.summaryDataGridViewTextBoxColumn.HeaderText = "Podsumowanie";
            this.summaryDataGridViewTextBoxColumn.Name = "summaryDataGridViewTextBoxColumn";
            // 
            // ratingDataGridViewTextBoxColumn
            // 
            this.ratingDataGridViewTextBoxColumn.DataPropertyName = "Rating";
            this.ratingDataGridViewTextBoxColumn.HeaderText = "Rating";
            this.ratingDataGridViewTextBoxColumn.Name = "ratingDataGridViewTextBoxColumn";
            // 
            // reviewerDataGridViewTextBoxColumn
            // 
            this.reviewerDataGridViewTextBoxColumn.DataPropertyName = "Reviewer";
            this.reviewerDataGridViewTextBoxColumn.HeaderText = "Recenzent";
            this.reviewerDataGridViewTextBoxColumn.Name = "reviewerDataGridViewTextBoxColumn";
            // 
            // dateTimeAddedDataGridViewTextBoxColumn
            // 
            this.dateTimeAddedDataGridViewTextBoxColumn.DataPropertyName = "DateTimeAdded";
            this.dateTimeAddedDataGridViewTextBoxColumn.HeaderText = "Czas dodania";
            this.dateTimeAddedDataGridViewTextBoxColumn.Name = "dateTimeAddedDataGridViewTextBoxColumn";
            // 
            // isRecommendedDataGridViewTextBoxColumn
            // 
            this.isRecommendedDataGridViewTextBoxColumn.DataPropertyName = "IsRecommended";
            this.isRecommendedDataGridViewTextBoxColumn.HeaderText = "Czy poleca?";
            this.isRecommendedDataGridViewTextBoxColumn.Name = "isRecommendedDataGridViewTextBoxColumn";
            // 
            // votesYesPercentDataGridViewTextBoxColumn
            // 
            this.votesYesPercentDataGridViewTextBoxColumn.DataPropertyName = "VotesYesPercent";
            this.votesYesPercentDataGridViewTextBoxColumn.HeaderText = "Procent głosów na tak";
            this.votesYesPercentDataGridViewTextBoxColumn.Name = "votesYesPercentDataGridViewTextBoxColumn";
            // 
            // votesYesDataGridViewTextBoxColumn
            // 
            this.votesYesDataGridViewTextBoxColumn.DataPropertyName = "VotesYes";
            this.votesYesDataGridViewTextBoxColumn.HeaderText = "Głosów na tak";
            this.votesYesDataGridViewTextBoxColumn.Name = "votesYesDataGridViewTextBoxColumn";
            // 
            // votesOverallDataGridViewTextBoxColumn
            // 
            this.votesOverallDataGridViewTextBoxColumn.DataPropertyName = "VotesOverall";
            this.votesOverallDataGridViewTextBoxColumn.HeaderText = "Głosów w ogóle";
            this.votesOverallDataGridViewTextBoxColumn.Name = "votesOverallDataGridViewTextBoxColumn";
            // 
            // reviewSourceDataGridViewTextBoxColumn
            // 
            this.reviewSourceDataGridViewTextBoxColumn.DataPropertyName = "ReviewSource";
            this.reviewSourceDataGridViewTextBoxColumn.HeaderText = "Źródło rezencji";
            this.reviewSourceDataGridViewTextBoxColumn.Name = "reviewSourceDataGridViewTextBoxColumn";
            // 
            // reviewToDisplayBindingSource
            // 
            this.reviewToDisplayBindingSource.DataSource = typeof(CeneoETLConsole.ReviewToDisplay);
            // 
            // productWithReviewListToDisplayBindingSource
            // 
            this.productWithReviewListToDisplayBindingSource.DataSource = typeof(CeneoETLConsole.ProductWithReviewListToDisplay);
            // 
            // productWithReviewListToDisplayBindingSource1
            // 
            this.productWithReviewListToDisplayBindingSource1.DataSource = typeof(CeneoETLConsole.ProductWithReviewListToDisplay);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 506);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Aplikacja ETL";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.reviewsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productsBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reviewDatabaseDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewDatabaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reviewToDisplayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productWithReviewListToDisplayBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productWithReviewListToDisplayBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource reviewDatabaseDataSetBindingSource;
        private ReviewDatabaseDataSet reviewDatabaseDataSet;
        private System.Windows.Forms.BindingSource productsBindingSource;
        private ReviewDatabaseDataSetTableAdapters.ProductsTableAdapter productsTableAdapter;
        private System.Windows.Forms.BindingSource reviewsBindingSource;
        private ReviewDatabaseDataSetTableAdapters.ReviewsTableAdapter reviewsTableAdapter;
        private System.Windows.Forms.BindingSource productWithReviewListToDisplayBindingSource;
        private System.Windows.Forms.BindingSource productWithReviewListToDisplayBindingSource1;
        private System.Windows.Forms.BindingSource reviewToDisplayBindingSource;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox ProductDisplaySummary;
        private System.Windows.Forms.TextBox ProductDisplayType;
        private System.Windows.Forms.TextBox ProductDisplayName;
        private System.Windows.Forms.TextBox ProductToDisplayTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ETLButtonOle;
        private System.Windows.Forms.Button LoadButtonOle;
        private System.Windows.Forms.Button TransformButtonOle;
        private System.Windows.Forms.Button ExtractButtonOle;
        private System.Windows.Forms.Label CeneoTextLabel;
        private System.Windows.Forms.Button ClearLogTextBox;
        private System.Windows.Forms.Button DeleteProductReviews;
        private System.Windows.Forms.TextBox LogTextBox;
        private System.Windows.Forms.TextBox ProductIdTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ETLButton;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button TransformButton;
        private System.Windows.Forms.Button ExtractButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.DataGridViewTextBoxColumn portalReviewIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn prosDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn consDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ratingDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reviewerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateTimeAddedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn isRecommendedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn votesYesPercentDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn votesYesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn votesOverallDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reviewSourceDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button ExportButton;
    }
}