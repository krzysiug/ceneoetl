﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace CeneoETLConsole
{
    public partial class Form1 : Form
    {
        Control myControl;
        ControlOle myControlOle;
        ProductWithReviewList myProductwithReviewList;
        ProductWithReviewList myProductwithReviewListOle;
        bool IfProductExist;
        ProductWithReviewListToDisplay objectToDisplay;

        public Form1()
        {
            myControl = new Control();
            myControlOle = new ControlOle();
            InitializeComponent();
            TransformButton.Enabled = false;
            LoadButton.Enabled = false;
            TransformButtonOle.Enabled = false;
            LoadButtonOle.Enabled = false;
            ExportButton.Enabled = false;
        }

        private void ExtractButton_Click(object sender, EventArgs e)
        {
            if (LogTextBox.Text.Length > 0)
                LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Extract dla portalu Ceneo");
            try
            {
                LogTextBox.AppendText(Environment.NewLine + DateTime.Now + " -> Pobrano " + myControl.Extract(ProductIdTextBox.Text) + " plik(i)" );
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Proces Extract dla portalu Ceneo zakończony powodzeniem");
                ExtractButton.Enabled = false;
                TransformButton.Enabled = true;
                ProductIdTextBox.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> " + ex.Message);
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Proces Extract dla portalu Ceneo zakończony NIEPOWODZENIEM");
            }
        }

        private void TransformButton_Click(object sender, EventArgs e)
        {
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Transform dla portalu Ceneo");
            myProductwithReviewList = myControl.Transform();
            TransformButton.Enabled = false;
            LoadButton.Enabled = true;
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Proces Transform dla portalu Ceneo zakończony powodzeniem");
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Load dla portalu Ceneo");
            
            int AddedReviews = myControl.Load(myProductwithReviewList, out IfProductExist);
            if (IfProductExist)
            {
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Produkt istnieje w bazie. Dodaje nowe opinie. ");
            }
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Dodano " + AddedReviews + " opinii o produkcie");
            ExtractButton.Enabled = true;
            TransformButton.Enabled = false;
            LoadButton.Enabled = false;
            ProductIdTextBox.Enabled = true;
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Proces Load dla portalu Ceneo zakończony powodzeniem");
        }

        private void ETLButton_Click(object sender, EventArgs e)
        {
            try {
                ExtractButton_Click(sender, e);
                TransformButton_Click(sender, e);
                LoadButton_Click(sender, e);
            }
            catch
            {

            }
        }

        private void ClearLogTextBox_Click(object sender, EventArgs e)
        {
            LogTextBox.Text = String.Empty;
        }

        private void DeleteProductReviews_Click(object sender, EventArgs e)
        {
            if (LogTextBox.Text.Length > 0)
                LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Usuwam obiekt id = " + ProductIdTextBox.Text);
            try {
                myControl.RemoveProductReviews(ProductIdTextBox.Text);
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Obiekt id = " + ProductIdTextBox.Text + " usunięty");
            }
            catch (Exception ex)
            {
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Błąd podczas usuwania produktu. Sprawdź ID produktu");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //GetWebpageOle myGetWebpageOle = new GetWebpageOle();
            //Debug.WriteLine(myGetWebpageOle.GetWebpageSource("37898478"));

            ControlOle myControlOle = new ControlOle();
            myControlOle.Extract(ProductIdTextBox.Text);

            //Console.WriteLine(HtmlPageParser.ReviewPagesNumberOle("39360269"));
        }

        private void ExtractButtonOle_Click(object sender, EventArgs e)
        {
            if (LogTextBox.Text.Length > 0)
                LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Extract dla portalu OleOle");
            try
            {
                LogTextBox.AppendText(Environment.NewLine + DateTime.Now + " -> Pobrano " + myControlOle.Extract(ProductIdTextBox.Text) + " plik(i)");
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Proces Extract dla portalu OleOle zakończony powodzeniem");
                ExtractButtonOle.Enabled = false;
                TransformButtonOle.Enabled = true;
                ProductIdTextBox.Enabled = false;
            }
            catch (Exception ex)
            {
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> " + ex.Message);
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Proces Extract dla portalu OleOle zakończony NIEPOWODZENIEM");
            }
        }

        private void TransformButtonOle_Click(object sender, EventArgs e)
        {
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Transform dla portalu OleOle");
            myProductwithReviewListOle = myControlOle.Transform();
            TransformButtonOle.Enabled = false;
            LoadButtonOle.Enabled = true;
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Proces Transform dla portalu OleOle zakończony powodzeniem");
            
        }

        private void LoadButtonOle_Click(object sender, EventArgs e)
        {
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Rozpoczynam proces Load dla portalu OleOle");

            int AddedReviews = myControlOle.Load(myProductwithReviewListOle, out IfProductExist);

            if (IfProductExist)
            {
                LogTextBox.AppendText(Environment.NewLine);
                LogTextBox.AppendText(DateTime.Now + " -> Produkt istnieje w bazie. Dodaje nowe opinie. ");
            }

            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Dodano " + AddedReviews + " opinii o produkcie");
            ExtractButtonOle.Enabled = true;
            TransformButtonOle.Enabled = false;
            LoadButtonOle.Enabled = false;
            ProductIdTextBox.Enabled = true;
            LogTextBox.AppendText(Environment.NewLine);
            LogTextBox.AppendText(DateTime.Now + " -> Proces Load dla portalu OleOle zakończony powodzeniem");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'reviewDatabaseDataSet.Reviews' table. You can move, or remove it, as needed.
            this.reviewsTableAdapter.Fill(this.reviewDatabaseDataSet.Reviews);
            // TODO: This line of code loads data into the 'reviewDatabaseDataSet.Products' table. You can move, or remove it, as needed.
            this.productsTableAdapter.Fill(this.reviewDatabaseDataSet.Products);

        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPageIndex == 0)
            {

                this.Size = new System.Drawing.Size(609, 545);
            }

            if (e.TabPageIndex == 1)
            {
                
                this.Size = new System.Drawing.Size(1320,700);
                tabPage2.Height = 1080;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProductWithReviewListToDisplay objectToDisplay = new ProductWithReviewListToDisplay("37516287");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            dataGridView1.RowHeadersVisible = false;
            try {
                objectToDisplay = new ProductWithReviewListToDisplay(ProductToDisplayTextBox.Text);
                ProductDisplayType.Text = objectToDisplay.Type;
                ProductDisplayName.Text = objectToDisplay.Model;
                ProductDisplaySummary.Text = objectToDisplay.Comments;
                dataGridView1.DataSource = objectToDisplay.Reviews;
                ExportButton.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " " + ProductToDisplayTextBox.Text);
                LogTextBox.AppendText(DateTime.Now + " -> " + ex.Message + " " + ProductToDisplayTextBox.Text);
            }
            
        }

        private void ETLButtonOle_Click(object sender, EventArgs e)
        {
            try {
                ExtractButtonOle_Click(sender, e);
                TransformButtonOle_Click(sender, e);
                LoadButtonOle_Click(sender, e);
            }
            catch
            {

            }
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            try{
                if (Directory.Exists("../Export/product" + objectToDisplay.PortalProductId + "export"))
                {
                    DirectoryInfo dir = new DirectoryInfo("../Export/product" + objectToDisplay.PortalProductId + "export");

                    foreach (FileInfo fi in dir.GetFiles())
                    {
                        fi.IsReadOnly = false;
                        fi.Delete();
                    }
                }

                DirectoryInfo dirNew = Directory.CreateDirectory("../Export/product" + objectToDisplay.PortalProductId + "export");
                
                System.IO.StreamWriter file = new System.IO.StreamWriter("../Export/product" + objectToDisplay.PortalProductId + "export/productData.txt" );

                file.WriteLine(objectToDisplay.ToString());
                file.Close();

                foreach (var review in objectToDisplay.Reviews)
                {
                    file = new System.IO.StreamWriter("../Export/product" + objectToDisplay.PortalProductId + "export/review" + review.PortalReviewId + ".txt");

                    file.WriteLine(review.ToString());
                    file.Close();
                }

                MessageBox.Show("Pomyślnie wyeksportowano opinie o produkcie");
                LogTextBox.AppendText(DateTime.Now + " -> Pomyślnie wyeksportowano opinie o produkcie " + objectToDisplay.PortalProductId);

            }
            catch
            {
                MessageBox.Show("Błąd podczas eksportu!");
                LogTextBox.AppendText(DateTime.Now + " -> Błąd podczas eksportu opinii o produkcie " + objectToDisplay.PortalProductId);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            string ReviewIdToDelete = dataGridView1.Rows[e.RowIndex].Cells["portalReviewIdDataGridViewTextBoxColumn"].Value.ToString();

            DialogResult dialogResult = MessageBox.Show("Czy na pewno chcesz usunąć recenzję o ID = " + ReviewIdToDelete, "Potwierdź usunięcie", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                using (var db = new ReviewDatabaseEntities())
                {
                    var ReviewToDelete = (from a in db.Reviews
                                          where a.PortalReviewId == ReviewIdToDelete
                                          select a).First();

                    db.Pros.RemoveRange(ReviewToDelete.Pros);
                    db.Cons.RemoveRange(ReviewToDelete.Cons);
                    db.Reviews.Remove(ReviewToDelete);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }
            }
            else if (dialogResult == DialogResult.No)
            {
            }

            
        }
    }
}
