Id recenzji z portalu: 4479370753
Zalety produktu: jakość prania ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka nowa więc nic z nią się nie dzieje. W codziennym użytkowaniu sprawdza się dobrze. Niestety podczas pierwszych ok 5 prań śmierdziało plastikami. Wada, którą zauważyłem to nieprecyzyjne przyciski dotykowe, czasem trzeba naciskać mocno a chwilę później są aż zbyt czułe. Uwaga na ekipy montujące! Pralkę wstawili byle jak nawet jej nie poziomując i zacisnęli przewód zasilający wodę.
Ocena produktu: 5
Nick oceniającego: mic
Czas dodania recenzji: 24.11.2013 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 100
Liczba osób uznających opinię za przydatną: 9
Liczba osób oceniająca recenzję: 9
Źródło recenzji: OleOle

