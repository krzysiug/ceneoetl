Id recenzji z portalu: 4776365083
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka o dużej pojemności i bardzo dużej mocy. Pobiera bardzo niewiele wody a mimo to, doskonale pierze. Zdecydowanym plusem jest bardzo przyjemna obsługa - jasne, polskie menu, nieskomplikowany system, bardzo przyjemne dla ucha dźwięki. Mimo ogromnej ilości obrotów - pralka wirując nie "wychodzi" tylko stoi stabilnie w miejscu (oczywiście przedmioty stojące na niej przesuwają się do samego brzegu, aczkolwiek nie spadają;). Nie jest też wcale głośna, nawet gdy wiruje na najwyższych obrotach. Bardzo się ucieszyłam kiedy się okazało, że aby spuścić z niej wodę wystarczy wyjąć małą rurkę i jej koniec wsadzić do miski - nie trzeba otwierać filtra, ani męczyć się z podkładaniem szmaty jak to było w starszych modelach pralek. Zrobiłam kilkanaście prań i póki co, jestem zachwycona!
Ocena produktu: 5
Nick oceniającego: Warszawianka
Czas dodania recenzji: 02.02.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 88
Liczba osób uznających opinię za przydatną: 7
Liczba osób oceniająca recenzję: 8
Źródło recenzji: OleOle

