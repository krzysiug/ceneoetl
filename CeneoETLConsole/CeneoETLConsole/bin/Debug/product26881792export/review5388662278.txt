Id recenzji z portalu: 5388662278
Zalety produktu: jakość prania ; pojemność ; programy ; stabilność ; 
Wady produktu: 
Podsumowanie recenzji: Po kilku tygodniach użytkowania mogę polecić tę pralkę innym. Jest pojemna, ma funkcje, które są dla mnie ważne i jak na razie nie znajduję wad. Fajny jest program do prania kurtek - nie są wymięte i nie trzeba ich prasować. Dźwięki wyłączyłam na samym początku i nic mi nie "pika" przy ustawianiu programów. Czytałam opinie, że ta pralka jest głośna - gdy zamkną drzwi do łazienki to prawie jej nie słychać więc dla mnie jest ok. Zamówiłam pralkę z wniesieniem, panowie przynieśli ją na drugie piętro, rozpakowali, powiedzieli co i jak podłączyć i jak zdjęć blokady i jeszcze posprzątali po sobie. Jedyne co było nie tak to brak podpisu na fakturze a na duplikat musiałam czekać dwa tygodnie, ale się doczekałam. Polecam.
Ocena produktu: 5
Nick oceniającego: Gosia
Czas dodania recenzji: 24.05.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

