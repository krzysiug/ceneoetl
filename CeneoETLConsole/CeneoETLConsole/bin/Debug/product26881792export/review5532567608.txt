Id recenzji z portalu: 5532567608
Zalety produktu: jakość prania ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Miły dla oka i czytelny wyświetlacz. Obsługa banalnie prosta. Ogólna jakość b.dobra, bez śmierdzących plastików. Pranie w końcu pachnie po wypraniu. Dość igłośna (buczenie silnika podczas prania) w porównaniu do poprzedniczki Whirpoola ładowanego z góry. Ale za to w tej widać co się dzieje w bębnie . Ostrzegam: przed wsunięciem jej przez drzwi łazienki o szerokości 60cm należy zdjąć drzwiczki (ja jeszcze zdjąłem górną pokrywę pralki) a i tak ledwo przeszła (trzeba uważać na pokrętło). Jest w sam raz dla 4 osobowej rodziny. Oby pochodziła jak najdłużej. Pralka "Made in POLAND".
Ocena produktu: 5
Nick oceniającego: Remy
Czas dodania recenzji: 16.06.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

