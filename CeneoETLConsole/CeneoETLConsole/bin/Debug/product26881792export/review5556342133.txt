Id recenzji z portalu: 5556342133
Zalety produktu: jakość prania ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: głośność pracy ; 
Podsumowanie recenzji: Pralka elegancka jeśli można tak określić wygląd pralki. Spełnia moje oczekiwania co do swoich możliwości. Największą wadą - i to było sporym zaskoczeniem - jest głośna praca! Każdemu ruchowi bębna towarzyszy dźwięk jakby głośnego tranformatoara. Na szczęście nie mam potrzeby używania pralki w godzinach nocnych czy wczesno-porannych :) Po kilku miesiącach użytkowania już przywykłem, że pralkę musi być słychać gdy pracuje: tzn gdy się zrobi cicho - oznacza koniec prania.
Ocena produktu: 5
Nick oceniającego: manks
Czas dodania recenzji: 20.06.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

