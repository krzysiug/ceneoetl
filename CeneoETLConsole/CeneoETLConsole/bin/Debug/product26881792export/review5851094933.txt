Id recenzji z portalu: 5851094933
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralkę używam od pół roku. Jak na razie bez zarzutu. Bardzo dobrze pierze, jest cicha, posiada wiele programów, łatwa obsługa, duży załadunek, fajny panel sterowania. Dodatkowa opcja w postaci "melodyjki" po zakończeniu prania bardzo przyjemna. Według mnie energooszczędna. Polecam.
Ocena produktu: 5
Nick oceniającego: ania
Czas dodania recenzji: 15.08.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

