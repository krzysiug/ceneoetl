Id recenzji z portalu: 7339730288
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka działa bardzo dobrze, nie mam zastrzeżeń. Przy doborze najważniejsze były dla mnie wysoka energooszczędność, duża pojemność, blokada rodzicielska - ten produkt spełniał wszystkie te wymagania w połączeniu z sensowną ceną. Dodatkowym plusem jest dla mnie spora elastyczność przy "ręcznym" doborze takich parametrów jak czas, temperatura i prędkość wirowania. Mały minus za wygląd, nie jest to najpiękniejsza wśród pralek (ale ten element był dla akurat najmniej istotny). Bezawaryjności nie jestem jeszcze w stanie ocenić, bo pralkę użytkuję od kilku tygodni. Jak zwykle obsługa sklepu na szóstkę.
Ocena produktu: 5
Nick oceniającego: naakve
Czas dodania recenzji: 21.04.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

