Id recenzji z portalu: 2477659
Zalety produktu: cena ; 
Wady produktu: niekt&#243;re egzemplarze wadliwe ; szybkość ; zepsuty firmware ; 
Podsumowanie recenzji: Trzymać się z daleka od Kingstona! System wstaje szybciej i programy uruchamiają się też szybciej, ale demon prędkości to to nie jest. Co więcej maksymalny odczyt sekwencyjny u mnie na SATA3 to jedynie 173MB/s, a zapis to tylko 75MB/s (tragicznie wolny HDD z laptopa ma 80MB/s!) i obie wartości spadły po upgrade firmware z 506 do 525. Nie wiem, czy Kingston sobie leci w kulki z klientami, czy dostałem uszkodzony dysk, ale na pewno będę trzymał się od tej firmy z daleka. Nie polecam nikomu, te dyski to czysta loteria, czasem dostanie się sprawny egzemplarz a czasem wadliwy. Lepiej dołożyć do Samsunga albo Intela i być pewnym jakości. 
Ocena produktu: 1
Nick oceniającego: Marcin
Czas dodania recenzji: 02.03.2014 21:56:58
Czy produkt jest rekomendowany: False
Procent osób uznających opinię za przydatną: 100
Liczba osób uznających opinię za przydatną: 7
Liczba osób oceniająca recenzję: 7
Źródło recenzji: Ceneo

