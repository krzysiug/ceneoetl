Id recenzji z portalu: 11022580131
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Jestem zadowolona z dużej pojemności pralki - 7kg. Dla 4-osobowej rodziny bardzo ważne, aby za jednym razem uprać pościel, ręczniki itp. Pralka nie jest bardzo głośna - w porównaniu z moją starą cicha:) Duży plus za wygląd, łatwość w obsłudze, intuicyjność.
Ocena produktu: 5
Nick oceniającego: Gosia
Czas dodania recenzji: 02.12.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

