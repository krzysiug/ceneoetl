Id recenzji z portalu: 11022854790
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka bardzo pozytywnie mnie zaskoczyła. Programator jest prosty w obsłudze - nawet dziecko sobie poradzi. Owszem nie ma programu na 30 min., ale można to zastąpić innym programem, np. pranie ręczne które trwa niecałą godzinę. Duży otwór do wkładania prania i duża pojemność to także działa na plus. Przy wirowaniu na najwyższych obrotach trochę ją słychać, ale wszystko jest w normie, a pranie też jest dobrze odwirowane.
Ocena produktu: 5
Nick oceniającego: gabi
Czas dodania recenzji: 02.12.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

