Id recenzji z portalu: 2422333
Zalety produktu: dobrze pierze ; dobrze wiruje (pranie prawie suche) ; 
Wady produktu: długie cykle prania ; hałas ; nieergonomiczny panel wyboru program&#243;w ; 
Podsumowanie recenzji: Pralka wizualnie nie budzi zastrzeżeń, dopóki się jej nie włączy. Panel sterowania jest nieergonomiczny - wybór programu zaznaczony jest przez elektroniczny wskaźnik (brak wskaźnika- zwykłej kreski na pokrętle w mojej opinii trochę utrudnia wybór cyklu prania). Ponadto pozostałe warianty wyboru - temp., wirowania, opcji prania zaznaczone są jedynie przez podświetlenie diody obok wariantu - powinien być podświetlony cały wariant np. 30, 40, 60 stopni lub cały elektroniczny panel. <br>
Pralka ma długie cykle prania w porównaniu z jej poprzedniczką firmy Bosch, i jest dość hałaśliwa. Dosyć denerwujący jest odgłos buczenia podczas pracy (prawie jak w spawarce).  Co do jakości prania nie mam zastrzeżeń.<br>
Kupiłam ją z uwagi na 5 letnie rozszerzone warunki gwarancji, które daje firma Samsung oraz dostępność do autoryzowanych serwisów w moim mieście. Niestety w przypadku Boscha nie ma serwisu w moim mieście, a 3 kolejne próby naprawy i ich koszty skłoniły mnie do nabycia nowej pralki.
Ocena produktu: 3
Nick oceniającego: Emila
Czas dodania recenzji: 20.01.2014 20:12:07
Czy produkt jest rekomendowany: False
Procent osób uznających opinię za przydatną: 82
Liczba osób uznających opinię za przydatną: 14
Liczba osób oceniająca recenzję: 17
Źródło recenzji: Ceneo

