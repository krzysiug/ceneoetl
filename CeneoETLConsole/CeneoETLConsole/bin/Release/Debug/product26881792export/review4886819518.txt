Id recenzji z portalu: 4886819518
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Do pralki póki co nie mam zastrzeżeń. Mankamentem tego i podobno innych modeli jest zapychający się przewód transportujący wodę z proszkiem z szufladki do bębna. Niestety już po kilku (dosłownie) praniach coś takiego się stało. Efektem jest zawsze zalanie łazienki. Także to jest mankament. Warto poczytać także opinie na forach. Obiektywnie rzecz ujmując nie jest to jakiś super wynalazek zważywszy na to, że około pięć lat temu kupiłem pralkę SAMSUNG wyglądającą dosyć podobnie a mającą porównywalne programy co ten model. Tamta stara miała nawet wyższą prędkość wirowania. Ale po pięciu latach się zepsuła i nie opłacało się jej naprawiać. Ogólnie, pralka jest w porządku.
Ocena produktu: 5
Nick oceniającego: ps
Czas dodania recenzji: 21.02.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 75
Liczba osób uznających opinię za przydatną: 3
Liczba osób oceniająca recenzję: 4
Źródło recenzji: OleOle

