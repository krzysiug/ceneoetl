Id recenzji z portalu: 4907869053
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka spełnia wszystkie moje oczekiwania, co prawda ciężko się wypowiedzieć na temat niezawodności sprzętu, jeżeli ma się go dopiero miesiąc i zrobiło się zaledwie 3 prania, ale jest cicha, wiadomo, przy wirowaniu trochę głośniejsza, dopiera wszystkie ubrania i ma wszystkie niezbędne funkcje potrzebne by wyprać sterty brudnych ciuchów :) Miłą i zabawną rzeczą, o której nie wiedziałam, to melodyjki które wygrywa przy włączeniu i jak skończy prać. Jeżeli kogoś drażni, można wyłączyć granie.
Ocena produktu: 5
Nick oceniającego: Marek
Czas dodania recenzji: 24.02.2014 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 75
Liczba osób uznających opinię za przydatną: 3
Liczba osób oceniająca recenzję: 4
Źródło recenzji: OleOle

