Id recenzji z portalu: 6989168788
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; energooszczędność ; 
Wady produktu: stabilność ; 
Podsumowanie recenzji: Generalnie pralka pierze dokładnie, programy przejrzyste, hałas w normie. Żona jest z niej bardzo zadowolona i podoba jej się również muzyka, która towarzyszy uruchamianiu :). Bardzo ważnym jest dokładne ustawienie pralki (wypoziomowanie), gdyż podczas wirowania mogą występować małe wibracje. Polecam tą pralkę !!!
Ocena produktu: 5
Nick oceniającego: Pusher
Czas dodania recenzji: 07.02.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

