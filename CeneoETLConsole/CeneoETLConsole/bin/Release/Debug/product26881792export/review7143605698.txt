Id recenzji z portalu: 7143605698
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka spodobała nam się, trudno się wypowiadać na temat niezawodności sprzętu (nie mam zaufania do dzisiejszego nowoczesnego sprzętu więc dokupiłam dodatkowe ubezpieczenie), pralkę mam od miesiąca i wykonałam zaledwie 5 prań. Pralka jest cicha w porównaniu z poprzednią (18 letni Zanussi) i bardzo dobrze wiruje. Posiada 15 minutowy program, który świetnie spełnia swoją rolę w praniu przepoconych ubrań, którymi szybko zapełnia się pojemnik na pranie. Grająca melodyjka po skończeniu prania oznajmia, że należy opróżnić pralkę.
Ocena produktu: 5
Nick oceniającego: GF
Czas dodania recenzji: 11.03.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

