Id recenzji z portalu: 7575757223
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Zrobiłam dotychczas kilkanaście prań na różnych programach - bez zarzutu. Pracuje dosyć cicho, jest bardzo pojemna. Prosta w obsłudze, programy można dostosować do swoich potrzeb, a można korzystać z ustawień fabrycznych. Bardzo fajnie.
Ocena produktu: 5
Nick oceniającego: A
Czas dodania recenzji: 14.06.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 50
Liczba osób uznających opinię za przydatną: 1
Liczba osób oceniająca recenzję: 2
Źródło recenzji: OleOle

