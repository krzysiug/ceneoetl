Id recenzji z portalu: 7638106588
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralkę mam niecały miesiąc, ale jestem zadowolona (porównuje do wcześniejszego modelu). Pranie jest doprane, nawet w niskich temperaturach. Duży wybór programów, możliwość własnych ustawień oraz pojemność bębna też są atutami tego modelu.
Ocena produktu: 5
Nick oceniającego: eta
Czas dodania recenzji: 25.06.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 67
Liczba osób uznających opinię za przydatną: 2
Liczba osób oceniająca recenzję: 3
Źródło recenzji: OleOle

