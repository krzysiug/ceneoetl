Id recenzji z portalu: 7705182353
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka bardzo fajna. W porównaniu do mojej poprzedniej (Wirpool ładowany od góry) to wręcz rewelacyjna! Przede wszystkim cicha, nawet podczas wirowania. Po skończonym praniu sama się wyłącza, więc nie marnuje prądu, jeśli nie musi. Bardzo prosta w obsłudze. Ma wiele przydatnych programów (np. kurtki, odplamianie), których parametry można modyfikować i dostosowywać do potrzeb. Wyświetlacz pokazuje czas pozostały do zakończenia prania, jest możliwość opóźnienia startu. Najczęściej stosuję program "pranie codzienne" (ok. 1 h) i jestem bardzo zadowolona z efektów prania. Jak dla mnie jedyny minus to gabaryty tej pralki, ale przy tak dużej ładowności da się to przeżyć ;)
Ocena produktu: 5
Nick oceniającego: anko
Czas dodania recenzji: 08.07.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 50
Liczba osób uznających opinię za przydatną: 1
Liczba osób oceniająca recenzję: 2
Źródło recenzji: OleOle

