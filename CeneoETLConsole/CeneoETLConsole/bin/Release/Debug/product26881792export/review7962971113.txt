Id recenzji z portalu: 7962971113
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Pralka spełnia całkowicie moje oczekiwania. Bardzo stabilna nawet podczas wirowania, jedynym drobnym mankamentem jest głośność pracy silnika. Rewelacyjny duży wsad, system eco bublee rzeczywiście działa nawet w niskich temperaturach. Bardzo przejrzyste menu, co prawda melodyjki z czasem mogą irytować, ale można je wyłączyć. Dużym plusem jest stosunkowo niewielki pobór wody, a mimo to dopiera bardzo dobrze nawet w niskich temperaturach. Bardzo łatwa w utrzymaniu w czystości nie ma problemu z wyczyszczeniem filtra czy szufladki na detergenty, co w przypadku mojej poprzedniej pralki było istnym koszmarem. Gorąco polecam ten produkt , kolejny Samsunga, z którego jestem zadowolona. Bardzo polecam również zakupy w oleole, które w moim przypadku przebiegły bez zarzutu.
Ocena produktu: 5
Nick oceniającego: goskinam1
Czas dodania recenzji: 21.08.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

