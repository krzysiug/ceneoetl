Id recenzji z portalu: 8023232998
Zalety produktu: jakość prania ; głośność pracy ; pojemność ; programy ; stabilność ; energooszczędność ; 
Wady produktu: 
Podsumowanie recenzji: Jestem bardzo zadowolona z zakupu. Pralka jest cicha, stabilna, świetnie pierze. Odpowiada mi czytelny, intuicyjny panel sterowania, nie trzeba kartkować instrukcji aby dopasować ustawienia do rodzaju prania. Wybór i rodzaj programów w pełni zadawala moje "pralnicze" potrzeby. Polecam gorąco!!!!
Ocena produktu: 5
Nick oceniającego: Dorota
Czas dodania recenzji: 01.09.2015 00:00:00
Czy produkt jest rekomendowany: True
Procent osób uznających opinię za przydatną: 0
Liczba osób uznających opinię za przydatną: 0
Liczba osób oceniająca recenzję: 0
Źródło recenzji: OleOle

